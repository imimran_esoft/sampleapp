/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import {MatDialog} from '@angular/material/dialog';
import { dialog_content_example_dialogComponent } from '../dialog_content_example_dialogComponent/dialog_content_example_dialog.component';

/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-ui_dialog',
    templateUrl: './ui_dialog.template.html'
})

export class ui_dialogComponent extends NBaseComponent implements OnInit {

    constructor(public dialog: MatDialog) {
        super();
    }

    ngOnInit() {

    }
    openDialog() {
        const dialogRef = this.dialog.open(dialog_content_example_dialogComponent);

        dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        });
    }
}
