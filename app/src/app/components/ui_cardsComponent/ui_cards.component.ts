/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';

/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-ui_cards',
    templateUrl: './ui_cards.template.html'
})

export class ui_cardsComponent extends NBaseComponent implements OnInit {
    cardImg = "https://material.angular.io/assets/img/examples/shiba1.jpg";
    cardImgThumb = "https://material.angular.io/assets/img/examples/shiba2.jpg";
    constructor() {
        super();
    }

    ngOnInit() {

    }
}
