/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import {FormBuilder, Validators} from '@angular/forms';
/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-home',
    templateUrl: './home.template.html'
})

export class homeComponent extends NBaseComponent implements OnInit {
    sampleForm;
    dataRole = ['admin', 'customer'];
    constructor(public fb: FormBuilder) {
        super();
    }

    ngOnInit() {
        this.sampleForm = this.fb.group({
            firstName: ['', [Validators.required]],
            lastName: ['',[Validators.required]],
            age: ['',[Validators.required]],
            role: ['',[Validators.required]]
        })
    }
    register() {
        console.log(this.sampleForm.value);
    }
}
