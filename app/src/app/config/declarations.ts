import { NeutrinosAuthGuardService } from 'neutrinos-oauth-client';
import { PageNotFoundComponent } from '../not-found.component';
import { LayoutComponent } from '../layout/layout.component';
import { ImgSrcDirective } from '../directives/imgSrc.directive';
import { APP_INITIALIZER } from '@angular/core';
import { NDataSourceService } from '../n-services/n-dataSorce.service';
import { environment } from '../../environments/environment';
import { NLocaleResource } from '../n-services/n-localeResources.service';
import { NAuthGuardService } from 'neutrinos-seed-services';
import { ArtImgSrcDirective } from '../directives/artImgSrc.directive';


window['neutrinos'] = {
  environments: environment
}

//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-uiformsComponent
import { uiformsComponent } from '../components/uiformsComponent/uiforms.component';
//CORE_REFERENCE_IMPORT-uiexpansionpanelComponent
import { uiexpansionpanelComponent } from '../components/uiexpansionpanelComponent/uiexpansionpanel.component';
//CORE_REFERENCE_IMPORT-uitableComponent
import { uitableComponent } from '../components/uitableComponent/uitable.component';
//CORE_REFERENCE_IMPORT-dialogoverviewexampletwoComponent
import { dialogoverviewexampletwoComponent } from '../components/dialogoverviewexampletwoComponent/dialogoverviewexampletwo.component';
//CORE_REFERENCE_IMPORT-dialog_content_example_dialogComponent
import { dialog_content_example_dialogComponent } from '../components/dialog_content_example_dialogComponent/dialog_content_example_dialog.component';
//CORE_REFERENCE_IMPORT-ui_dialogComponent
import { ui_dialogComponent } from '../components/ui_dialogComponent/ui_dialog.component';
//CORE_REFERENCE_IMPORT-ui_check_boxComponent
import { ui_check_boxComponent } from '../components/ui_check_boxComponent/ui_check_box.component';
//CORE_REFERENCE_IMPORT-ui_cardsComponent
import { ui_cardsComponent } from '../components/ui_cardsComponent/ui_cards.component';
//CORE_REFERENCE_IMPORT-ui_buttonsComponent
import { ui_buttonsComponent } from '../components/ui_buttonsComponent/ui_buttons.component';
//CORE_REFERENCE_IMPORT-homeComponent
import { homeComponent } from '../components/homeComponent/home.component';

/**
 * Reads datasource object and injects the datasource object into window object
 * Injects the imported environment object into the window object
 *
 */
export function startupServiceFactory(startupService: NDataSourceService) {
  return () => startupService.getDataSource();
}

/**
*bootstrap for @NgModule
*/
export const appBootstrap: any = [
  LayoutComponent,
];


/**
*declarations for @NgModule
*/
export const appDeclarations = [
  ImgSrcDirective,
  LayoutComponent,
  PageNotFoundComponent,
  ArtImgSrcDirective,
  //CORE_REFERENCE_PUSH_TO_DEC_ARRAY
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-uiformsComponent
uiformsComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-uiexpansionpanelComponent
uiexpansionpanelComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-uitableComponent
uitableComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-dialogoverviewexampletwoComponent
dialogoverviewexampletwoComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-dialog_content_example_dialogComponent
dialog_content_example_dialogComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-ui_dialogComponent
ui_dialogComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-ui_check_boxComponent
ui_check_boxComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-ui_cardsComponent
ui_cardsComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-ui_buttonsComponent
ui_buttonsComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-homeComponent
homeComponent,

];

/**
* provider for @NgModuke
*/
export const appProviders = [
  NDataSourceService,
  NLocaleResource,
  {
    // Provider for APP_INITIALIZER
    provide: APP_INITIALIZER,
    useFactory: startupServiceFactory,
    deps: [NDataSourceService],
    multi: true
  },
  NAuthGuardService,
  //CORE_REFERENCE_PUSH_TO_PRO_ARRAY

];

/**
* Routes available for bApp
*/

// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_START
export const appRoutes = [{path: 'home', component: homeComponent},{path: 'ui_buttons', component: ui_buttonsComponent},{path: 'ui_cards', component: ui_cardsComponent},{path: 'ui_check_box', component: ui_check_boxComponent},{path: 'ui_dialog', component: ui_dialogComponent},{path: 'ui-table', component: uitableComponent},{path: 'ui-expansion-panel', component: uiexpansionpanelComponent},{path: 'ui-forms', component: uiformsComponent},{path: '', redirectTo: 'home', pathMatch: 'full'},{path: '**', component: PageNotFoundComponent}]
// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_END
